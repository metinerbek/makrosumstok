<?php 

class Stok extends CI_Controller{
	
	
	
function __construct(){
parent::__construct();		
	$this->load->model("stokmodel");
}	
	
function index(){
	$data["renkler"]=$this->stokmodel->makepairvaluetooptions($this->stokmodel->getcolors());
	$data["gruplar"]=$this->stokmodel->makepairvaluetooptions($this->stokmodel->getgroups());
	$data["stoklar"]=$this->stokmodel->getstok();
	$this->load->view("stok_yonet",$data);
}	

function ilegoreilce($il){

echo $this->stokmodel->makepairvaluetooptions($this->stokmodel->getilceler($il));
	
}


function stok_ekle(){
	
$urun=array(
'stok_name'=>$this->input->post("stok_name"),
'stok_group_id'=>$this->input->post("stok_group_id"),
'stok_color_id'=>$this->input->post("stok_color_id"),
'stok_urun_alis_fiyati'=>$this->input->post("stok_urun_alis_fiyati"),
'stok_urun_satis_fiyati'=>$this->input->post("stok_urun_satis_fiyati"),
);		

echo json_encode($this->stokmodel->stok_ekle($urun));
	
}

function stok_duzenle($stok_id){
	
$urun=array(
'stok_name'=>$this->input->post("stok_name"),
'stok_group_id'=>$this->input->post("stok_group_id"),
'stok_color_id'=>$this->input->post("stok_color_id"),
'stok_urun_alis_fiyati'=>$this->input->post("stok_urun_alis_fiyati"),
'stok_urun_satis_fiyati'=>$this->input->post("stok_urun_satis_fiyati"),
);		

echo json_encode($this->stokmodel->stok_duzenle($urun,$stok_id));
}

function getstok($stok_id){

$stok=$this->stokmodel->getstok($stok_id);
if(count($stok)>0){
echo json_encode(array('snc'=>true,'stok'=>$stok[0],'mesaj'=>''));	
}else{
echo json_encode(array('snc'=>false,'stok'=>'','mesaj'=>'Stok Bulunamadư.'));
}
	
}





function getstoklist($stok_id=''){
$stoklar=$this->stokmodel->getstok($stok_id);	
echo $this->load->view("stok/stoklist",array('stoklar'=>$stoklar),true);		
}


function stok_sil($stok_id){
echo json_encode($this->stokmodel->stok_sil($stok_id));	
	
	
}








}

?>