<?php

class stokkontrol extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("stokmodel");
    }

    function index() {
        $data["iller"]=$this->stokmodel->makepairvaluetooptions($this->stokmodel->getiller());
        $data["toplamhareketler"] = $this->stokmodel->getstokhareketi('', 1);
        $this->load->view("stok_yonetim_index", $data);
    }

    function getgenelrapor() {

        foreach ($this->stokmodel->getstokhareketi('', 1) as $hareketrapor) {
            $this->load->view("stok_hareketleri/temelhareketlistpart", array('hareketrapor' => $hareketrapor));
        }
        
    }

}

?>
