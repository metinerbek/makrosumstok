<?php 
class musteri extends CI_Controller{
	
function __construct(){
parent::__construct();
	$this->load->model("mmusteri");
	$this->load->model("stokmodel");		
}	
	
function index(){
	$data["iller"]=$this->stokmodel->makepairvaluetooptions($this->stokmodel->getiller());
	$data["ilceler"]=$this->stokmodel->makepairvaluetooptions($this->stokmodel->getilceler());
	$data["musteriler"]=$this->mmusteri->getmusteri();
	$this->load->view("musteri_yonetimi",$data);

	
}	
	
function musteri_ekle(){
	
$musteri=array(
'musteri_name'=>$this->input->post("musteri_adi"),
'musteri_tel'=>$this->input->post("musteri_tel"),
'musteri_il'=>$this->input->post("musteri_il"),
'musteri_ilce'=>$this->input->post("musteri_ilce"),
'musteri_adres'=>$this->input->post("musteri_adres"),
'kayit_tarihi'=>strtotime(date("d.m.Y"))
);		

echo json_encode($this->mmusteri->yenimusteriekle($musteri));
	
}


function getmusteri($musteri_id){

$musteri=$this->mmusteri->getmusteri($musteri_id);
if(count($musteri)>0){
echo json_encode(array('snc'=>true,'musteri'=>$musteri[0],'mesaj'=>''));	
}else{
echo json_encode(array('snc'=>false,'musteri'=>'','mesaj'=>'M��teri Bulunamad�.'));
}
	
}
function musteri_duzenle($musteri_id){
$musteri=array(
'musteri_name'=>$this->input->post("musteri_adi"),
'musteri_tel'=>$this->input->post("musteri_tel"),
'musteri_il'=>$this->input->post("musteri_il"),
'musteri_ilce'=>$this->input->post("musteri_ilce"),
'musteri_adres'=>$this->input->post("musteri_adres"),

);		

echo json_encode($this->mmusteri->musteri_duzenle($musteri,$musteri_id));	
	
	
}

function getmusterilist($musteri_id=''){
$musteriler=$this->mmusteri->getmusteri($musteri_id);	
echo $this->load->view("musteri/musterilist",array('musteriler'=>$musteriler),true);	
	
}


function musteri_sil($musteri_id){
echo json_encode($this->mmusteri->musteri_sil($musteri_id));	
	
	
}

function getmusteriozet($musteri_id){
    
$this->load->view("musteri/musterihareketozet",array('hesaphareketliligi'=>$this->stokmodel->getstokhareketi('',2,$musteri_id))) ; 
    
}










}


?>