<?php 

class Stokhareketleri extends CI_Controller{
	
	
	
function __construct(){
parent::__construct();		
	$this->load->model("stokmodel");
	$this->load->model("mmusteri");
	
}	
	
function index(){
	$data["musteriler"]=$this->stokmodel->makepairvaluetooptions($this->mmusteri->getmusteri());
	$data["urunler"]=$this->stokmodel->makepairvaluetooptions($this->stokmodel->getstok());
	
	$data["stokhareketleri"]=$this->stokmodel->getstokhareketi();
	
	
	
	
	$this->load->view("stok_hareketleri",$data);
}	


function toplamfiyatdondur($urun_id='',$miktar='',$islem=''){
	echo json_encode($this->stokmodel->fiyathesapla($urun_id,$miktar,$islem));
}

function stok_hareketi_ekle(){
	
$hareket=array(
'stok_event_customer'=>$this->input->post("stok_event_customer"),
'stok_event_product_id'=>$this->input->post("stok_event_product_id"),
'stok_event_product_count'=>$this->input->post("stok_event_product_count"),
'stok_event_type'=>$this->input->post("stok_event_type"),
'stok_event_price'=>$this->stokmodel->fiyathesapla($this->input->post("stok_event_product_id"),$this->input->post("stok_event_product_count"),$this->input->post("stok_event_type"))["toplam"],
'stok_event_date'=>strtotime(date("d.m.Y"))
);		

echo json_encode($this->stokmodel->stok_hareketi_ekle($hareket));
}


function getstokhareketleri($stok_events_id=''){
$stokhareketleri=$this->stokmodel->getstokhareketi($stok_events_id);	
echo $this->load->view("stok_hareketleri/stokhareketlist",array('stokhareketleri'=>$stokhareketleri),true);	
	
}
}

?>