<?php 
class mmusteri extends CI_Model{
	
function yenimusteriekle($musteri){
	
if(!empty($musteri["musteri_name"])){
if($this->db->insert("customers",$musteri)){
return array('snc'=>true,'musteri_id'=>$this->db->insert_id(),'musteri'=>$musteri,'mesaj'=>"Müşteri Kaydedildi");
}else{
return array('snc'=>false,'musteri_id'=>'','musteri'=>'','mesaj'=>"");
}
}else{
return array('snc'=>false,'musteri_id'=>'','musteri'=>'','mesaj'=>"Zorunlu kısımları boş geçtiniz.");	
}
	
}	
	
function getmusteri($musteri_id=''){
$this->db->select("customers.*,il.il_adi,ilce.ilce_adi");
if($musteri_id!=''){

$this->db->where('musteri_id',$musteri_id);	
}

if($this->input->post("kayit_baslangic_tarihi") and $this->input->post("kayit_bitis_tarihi")){

 
$this->db->where("customers.kayit_tarihi>=",strtotime($this->input->post("kayit_baslangic_tarihi")." 23:59:59"));  
$this->db->where("customers.kayit_tarihi<=",strtotime($this->input->post("kayit_bitis_tarihi")." 23:59:59")); 
}
$this->db->where('musteri_durum',"0");	

$this->db->join("il","il.il_id=customers.musteri_il","left");
$this->db->join("ilce","ilce.ilce_id=customers.musteri_ilce","left");
	
$musterirs=$this->db->get("customers");

$musteriler=array();

foreach($musterirs->result_array() as $musteri){
$musteri["text"]=$musteri["musteri_name"];
$musteri["value"]=$musteri["musteri_id"];
array_push($musteriler,$musteri);		
}	
	
return $musteriler;	
		
}




function musteri_duzenle($musteri,$musteri_id){
	
if(!empty($musteri_id)){
$this->db->where("musteri_id",$musteri_id);
if($this->db->get("customers")->num_rows()>0){
$this->db->where("musteri_id",$musteri_id);
if($this->db->update("customers",$musteri)){
return array('snc'=>true,'musteri_id'=>$musteri_id,'musteri'=>$musteri,'mesaj'=>"Müşteri Düzenlendi");	
}else{
return array('snc'=>false,'musteri_id'=>'','musteri'=>'','mesaj'=>"");
}


}else{
return array('snc'=>false,'musteri_id'=>'','musteri'=>'','mesaj'=>"Müşteri Bulunamadı");
}
}else{
return array('snc'=>false,'musteri_id'=>'','musteri'=>'','mesaj'=>"Zorunlu kısımları boş geçtiniz.");	
}
	
}	




function musteri_sil($musteri_id){
if(!empty($musteri_id)){
$this->db->where("musteri_id",$musteri_id);
$this->db->where("musteri_durum","0");

if($this->db->get("customers")->num_rows()>0){
$this->db->where("musteri_id",$musteri_id);
if($this->db->update("customers",array('musteri_durum'=>"1"))){
return array('snc'=>true,'mesaj'=>"Müşteri Silindi");	
}else{
return array('snc'=>false,'mesaj'=>"");
}


}else{
return array('snc'=>false,'mesaj'=>"Müşteri Bulunamadı");
}
}else{
return array('snc'=>false,'mesaj'=>"Zorunlu kısımları boş geçtiniz.");	
}	
	
}


	
	
}


?>