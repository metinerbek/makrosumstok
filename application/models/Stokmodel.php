<?php 
class stokmodel extends CI_Model{
	
function getilceler($il_id='',$ilce_id=''){
$ilceler=array();
$ilceleroptions="";
if($il_id!=''){
$this->db->where("il_kodu",$il_id);
}

$gelenilcelerrs=$this->db->get("ilce");	
foreach($gelenilcelerrs->result() as $ilce){
//$ilceleroptions.='<option value='.$ilce->ilce_id.'>'.$ilce->ilce_adi.'</option>'	
array_push($ilceler,array('text'=>$ilce->ilce_adi,'value'=>$ilce->ilce_id));

}	
return $ilceler;	
}	
	
	
function getiller($il_id=''){
$iller=array();	
$illerrs=$this->db->get("il");	
foreach($illerrs->result() as $il){
array_push($iller,array('text'=>$il->il_adi,'value'=>$il->il_id));
}
return $iller;

	
}	
	
function makepairvaluetooptions($pairvaluedizi){
$textim="";
foreach($pairvaluedizi as $deger){
$textim.='<option value="'.$deger["value"].'">'.$deger["text"].'</option>';
}		
return $textim;	
}




function getstok($stok_id=''){
$this->db->select("stok.*,stok_groups.stok_group_name,stok_colors.stok_color_name");
if($stok_id!=''){
$this->db->where('stok_id',$stok_id);	
}	
$this->db->where('stok_urun_durum',"0");	


if($this->input->post("search_product_name")){
$this->db->like("stok_name",$this->input->post("search_product_name"));       
}
if($this->input->post("search_stok_color_id")){
$this->db->where("stok_color_id",$this->input->post("search_stok_color_id"));      
}
if($this->input->post("search_stok_group_id")){
$this->db->where("stok_group_id",$this->input->post("search_stok_group_id"));   	
}



$this->db->join("stok_groups","stok_groups.stok_groups_id=stok.stok_group_id","left");
$this->db->join("stok_colors","stok_colors.stok_colors_id=stok.stok_color_id","left");
	
$stokrs=$this->db->get("stok");
$stokuruns=array();

foreach($stokrs->result_array() as $urun){
	
$urun["text"]=$urun["stok_name"];
$urun["value"]=$urun["stok_id"];	
	
	
array_push($stokuruns,$urun);		
}	
	
return $stokuruns;	
		
}



function getcolors($stok_colors_id=''){
$colors=array();
if($stok_colors_id!=''){
	$this->db->where("stok_colors_id",$stok_colors_id);
}	
$colorrs=$this->db->get("stok_colors");	
foreach($colorrs->result() as $color){
array_push($colors,array('text'=>$color->stok_color_name,'value'=>$color->stok_colors_id));
}
return $colors;	
	
}
	

function getgroups($stok_groups_id=''){
$groups=array();
if($stok_groups_id!=''){
	$this->db->where("stok_groups_id",$stok_groups_id);
}	
$groupsrs=$this->db->get("stok_groups");	
foreach($groupsrs->result() as $group){
array_push($groups,array('text'=>$group->stok_group_name,'value'=>$group->stok_groups_id));
}
return $groups;	
	
}


function stok_ekle($urun){
	
if(!empty($urun["stok_name"])){
if($this->db->insert("stok",$urun)){
return array('snc'=>true,'stok_id'=>$this->db->insert_id(),'urun'=>$urun,'mesaj'=>"Stoğa Ürün Kaydedildi");
}else{
return array('snc'=>false,'stok_id'=>'','urun'=>'','mesaj'=>"");
}
}else{
return array('snc'=>false,'stok_id'=>'','urun'=>'','mesaj'=>"Zorunlu kısımları boş geçtiniz.");	
}
	
}



function stok_duzenle($urun,$stok_id){
	
if(!empty($stok_id)){
$this->db->where("stok_id",$stok_id);
if($this->db->get("stok")->num_rows()>0){
$this->db->where("stok_id",$stok_id);
if($this->db->update("stok",$urun)){
return array('snc'=>true,'stok_id'=>$stok_id,'urun'=>$urun,'mesaj'=>"Stok Düzenlendi");	
}else{
return array('snc'=>false,'stok_id'=>'','urun'=>'','mesaj'=>"");
}


}else{
return array('snc'=>false,'stok_id'=>'','urun'=>'','mesaj'=>"Stok Bulunamadı");
}
}else{
return array('snc'=>false,'stok_id'=>'','urun'=>'','mesaj'=>"Zorunlu kısımları boş geçtiniz.");	
}
	
}	


function stok_sil($stok_id){
if(!empty($stok_id)){
$this->db->where("stok_id",$stok_id);
$this->db->where("stok_urun_durum","0");

if($this->db->get("stok")->num_rows()>0){
$this->db->where("stok_id",$stok_id);
if($this->db->update("stok",array('stok_urun_durum'=>"1"))){
return array('snc'=>true,'mesaj'=>"Stok Silindi");	
}else{
return array('snc'=>false,'mesaj'=>"");
}


}else{
return array('snc'=>false,'mesaj'=>"Stok Bulunamadı");
}
}else{
return array('snc'=>false,'mesaj'=>"Zorunlu kısımları boş geçtiniz.");	
}	
	
}	

	
	
function fiyathesapla($urun_id,$miktar,$islem){
$uruns=$this->getstok($urun_id);	
if(count($uruns)>0){
$urun=$uruns[0];
if($islem==1){
return array('snc'=>true,'toplam'=>$urun["stok_urun_alis_fiyati"]*$miktar,'mesaj'=>'');
}else{
return array('snc'=>true,'toplam'=>$urun["stok_urun_satis_fiyati"]*$miktar,'mesaj'=>'');
}


}else{
	return array('snc'=>true,'toplam'=>0,'mesaj'=>'');
}	
	
	
}	
	
	



function stok_hareketi_ekle($hareket){
	
if(!empty($hareket["stok_event_product_id"])){
if($this->db->insert("stok_events",$hareket)){
return array('snc'=>true,'stok_events_id'=>$this->db->insert_id(),'hareket'=>$hareket,'mesaj'=>"Stok Hareketi Kaydedildi");
}else{
return array('snc'=>false,'stok_events_id'=>'','hareket'=>'','mesaj'=>"");
}
}else{
return array('snc'=>false,'stok_events_id'=>'','hareket'=>'','mesaj'=>"Zorunlu kısımları boş geçtiniz."+$hareket["stok_event_product_id"]);	
}
	
}





function getstokhareketi($stok_events_id='',$genelRaporMu=0,$musteriId=0){
$ekselect="";  
if($genelRaporMu==1){
 
 $this->db->group_by("stok_event_product_id");
 if($this->input->post("stokdurum_baslangic_tarihi") and $this->input->post("stokdurum_bitis_tarihi")){
  $this->db->where('stok_event_date>=',strtotime($this->input->post("stokdurum_baslangic_tarihi")." 23:59:59"));
  $this->db->where('stok_event_date<=',strtotime($this->input->post("stokdurum_bitis_tarihi")." 23:59:59"));
}
 if($this->input->post("stokencoksatilan_sehir")){
  $this->db->where("musteri_il",$this->input->post("stokencoksatilan_sehir"));
 } 
 $ekselect=",SUM(stok_event_type*stok_event_product_count) as stok_durum_toplam,SUM(((stok_event_type+1)/2)*stok_event_product_count) as satistoplammiktar  ";
 $this->db->order_by("satistoplammiktar","desc");
 
 

 
 
}else if($genelRaporMu==2){
$this->db->group_by("stok_event_date");
$this->db->where("stok_event_customer",$musteriId);
 
 $ekselect=",SUM(stok_event_type*stok_event_price) as toplamtutar";    
    
    
    
}    
   // 
$this->db->select("stok_events.*,customers.musteri_name,customers.musteri_il,stok.*".$ekselect);
//
if($stok_events_id!=''){
$this->db->where('stok_events_id',$stok_events_id);	
}
// and is_integer($this->input->post("limit_res"))
if($this->input->post("limit_res") and is_numeric($this->input->post("limit_res"))){
    $this->db->limit((int)$this->input->post("limit_res"));
    
}

//$this->db->where('stok_urun_durum',"0");	
$this->db->join("stok","stok.stok_id=stok_events.stok_event_product_id","left");
$this->db->join("customers","customers.musteri_id=stok_events.stok_event_customer","left");
/*
$this->db->join("stok_groups","stok_groups.stok_groups_id=stok.stok_group_id","left");
$this->db->join("stok_colors","stok_colors.stok_colors_id=stok.stok_color_id","left");
*/



$stokhareketirs=$this->db->get("stok_events");
//echo $this->db->last_query();
$stok_hareketleri=array();

foreach($stokhareketirs->result_array() as $stok_hareketi){

	
array_push($stok_hareketleri,$stok_hareketi);		
}	
	
return $stok_hareketleri;	
		
}
	
	

	
}

?>