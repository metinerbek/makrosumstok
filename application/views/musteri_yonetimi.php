<!DOCTYPE HTML>

<html >
  <head>
    <meta charset="utf-8">
    <title>Müşteri Yönetimi</title>
<?php $this->load->view("standart");?>
<script type="text/javascript" src="<?php echo base_url().'js/jquery.ui.datepicker-tr.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url().'js/musteri_yonetimi.js';?>"></script>
<script language="javascript">
var site="<?php echo site_url();?>";

$(document).ready(function(){
	
//$(".datepicker").datepicker();		
	
	
$("#musteri_il").click(function(){
ilegoreilce($("#musteri_il").val(),'musteri_ilce');
});	

$("#kayit_baslangic_tarihi").change(function(){
if($("#kayit_baslangic_tarihi").val()==""){
	$("#kayit_bitis_tarihi").val("");
	$("#kayit_bitis_tarihi").prop('disabled', true);
}
});	



$("#kayit_bitis_tarihi").prop('disabled', true);
$("#kayit_baslangic_tarihi").datepicker({
onSelect: function(dateText) {
        $sD = new Date(dateText);
        
		$("#kayit_bitis_tarihi").datepicker();
		$("#kayit_bitis_tarihi").datepicker('option','minDate',dateText);
		$("#kayit_bitis_tarihi").prop('disabled', false);
    }	
});


	
});


</script>
<style>
body {
padding-top:50px;	
width:80%;
padding-left:10%;
}
.eklebtn{
	padding-bottom:15px;
}
</style>
  </head>

  <body>
<?php $this->load->view("menu");?>
<div class="container-fluid">
    <div style="text-align:center;">
        <h4><u>Müşteri Yönetimi</u></h4>
    </div>
	<div class="row-fluid" style="">
	<div id="baslik">
		<div class="eklebtn">
			<a class="btn btn-info btn-large" href="javascript:void(0)" onclick="musteriekleduzenle('Ekle')"> Müşteri Ekle  </a>
                &nbsp;<a href="<?php echo site_url();?>" class="btn btn-success btn-large">Ana Panele Dön</a>&nbsp;&nbsp;
		</div>
		
		<div class="searchbox">
			<form id="filtrefrm">
				Kayıt Tarihi : <input type="text" name="kayit_baslangic_tarihi" id="kayit_baslangic_tarihi" class="datepicker"> &nbsp; - &nbsp;
				<input type="text" name="kayit_bitis_tarihi" id="kayit_bitis_tarihi" class="datepicker">
				
				<input type="button" class="btn-primary" value="Filtrele" onclick="listeyenile(3,'')">
				
			</form>
		</div>
		
	</div>	
	<div id="musterilist">
		<table class="table table-bordered">
			<tr class="info">
				<td>Müşteri ID</td>
				<td>Müşteri Adı</td>
				<td>Müşteri Tel</td>
				<td>Müşteri Adres</td>
				<td>Müşteri Kayıt Tarihi</td>
                                <td>Müşteri Hesap Özeti</td>
				<td>Müşteri Düzenle</td>
				<td>Müşteri Sil</td>
			</tr>
			<tbody id="musterilistesi">
			<?php 
			$this->load->view("musteri/musterilist",array('musteriler'=>$musteriler));
			?>
			</tbody>
		</table>
	
	</div>
		
</div>

</div>
<?php $this->load->view("dialogs/musteriekledialog");?>
  <div id="musteriozet" style="display:none;"></div>
</body>
</html>