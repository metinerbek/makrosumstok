<!DOCTYPE HTML>

<html>
    <head>
        <meta charset="utf-8">
    <title>Stok Yönetimi</title>
    <?php $this->load->view("standart"); ?>
    <script type="text/javascript" src="<?php echo base_url() . 'js/stok_hareket_list.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'js/jquery.ui.datepicker-tr.js';?>"></script>
    
    <script language="javascript">
        var site = "<?php echo site_url(); ?>";

        $(document).ready(function () {

            $("#stokdurum_baslangic_tarihi").change(function () {
                if ($("#stokdurum_baslangic_tarihi").val() == "") {
                    $("#stokdurum_bitis_tarihi").val("");
                    $("#stokdurum_bitis_tarihi").prop('disabled', true);
                }
            });



            $("#stokdurum_bitis_tarihi").prop('disabled', true);
            $("#stokdurum_baslangic_tarihi").datepicker({
                onSelect: function (dateText) {
                    $sD = new Date(dateText);

                    $("#stokdurum_bitis_tarihi").datepicker();
                    $("#stokdurum_bitis_tarihi").datepicker('option', 'minDate', dateText);
                    $("#stokdurum_bitis_tarihi").prop('disabled', false);
                }
            });



        });












    </script>
</head>
<body>


<div style="text-align: center;padding-top:50px;" id="content">
    <a href="<?php echo site_url("/stok"); ?>" class="btn btn-warning btn-large">Stok Ürünleri Yönetimi</a>&nbsp;&nbsp;
    <a href="<?php echo site_url("/musteri"); ?>" class="btn btn-primary btn-large">Müşteri Yönetimi</a>&nbsp;&nbsp;
    <a href="<?php echo site_url("/stokhareketleri"); ?>" class="btn btn-info btn-large">Stok Hareketleri Yönetimi</a>&nbsp;&nbsp;
</div>
<br><br>
<div style="width:70%;padding-left:15%;">
    <?php
    $this->load->view("stok_hareketleri/temelhareketlist", array('toplamhareketler' => $toplamhareketler));
    ?>
</div>

<!--
<div style="width:60%;padding-left:20%;">
    <?php
    $this->load->view("stok_hareketleri/temelhareketlistencoksatilan", array('toplamhareketler' => $toplamhareketler));
    ?>
</div>
--->
</body>
</html>