<!DOCTYPE HTML>

<html >
    <head>
        <meta charset="utf-8">
    <title>Stok Yönetimi</title>
    <?php $this->load->view("standart"); ?>
    <script type="text/javascript" src="<?php echo base_url() . 'js/stokyonet.js'; ?>"></script>	
    <script language="javascript">
        var site = "<?php echo site_url(); ?>";

        $(document).ready(function () {


        });


        function ilegoreilce(id, duzenlemesncid) {

            $.ajax({
                url: site + '/stok/ilegoreilce/' + id,
                type: "POST",
                success: function (cikti) {

                    $("#" + duzenlemesncid).html(cikti);

                }



            });




        }









    </script>
    <style>
        body {
            padding-top:50px;	
            width:80%;
            padding-left:10%;
        }
        .eklebtn{
            padding-bottom:15px;
        }
    </style>
</head>

<body>
    <?php //$this->load->view("menusecimi"); ?>
<div class="container-fluid">
      <div style="text-align:center;">
        <h4><u>Stok Yönetimi</u></h4>
    </div>
    <div class="row-fluid" style="">
        <div id="baslik">
            <div class="eklebtn">
                <a class="btn btn-info btn-large" href="javascript:void(0)" onclick="stokekleduzenle('Ekle')"> Stok Ürün Ekle  </a>
                        &nbsp;<a href="<?php echo site_url();?>" class="btn btn-success btn-large">Ana Panele Dön</a>&nbsp;&nbsp;
            </div>
        </div>	


        <div class="searchbox">
            <form id="filtrefrm">
                Ürün Adı :  <input type="text" name="search_product_name" id="search_product_name" class=""> &nbsp; 
                    Ürün Grubu :
                    <select name="search_stok_group_id" id="search_stok_group_id">
                        <option value="">- Seçiniz -</option>
                        <?php echo $gruplar; ?>
                    </select>
                    &nbsp;  Ürün Rengi : 
                    <select name="search_stok_color_id" id="search_stok_color_id">
                        <option value="">- Seçiniz -</option>
                        <?php echo $renkler; ?>
                    </select>
                    &nbsp;
                    <input type="button" class="btn-primary" value="Filtrele" onclick="listeyenile(3, '')">

                        </form>
                        </div>
                        <div id="stoklistem">

                            <table class="table table-bordered">
                                <tr class="info">
                                    <td>Ürün ID</td>
                                    <td>Ürün Adı</td>
                                    <td>Ürün Grubu</td>
                                    <td>Ürün Rengi</td>
                                    <td>Ürün Alış Fiyatı</td>
                                    <td>Ürün Satış Fiyatı</td>
                                    <td>Ürün Düzenle</td>
                                    <td>Ürün Sil</td>
                                </tr>
                                <tbody id="stoklistesi">
                                    <?php
                                    $this->load->view("stok/stoklist", array('stoklar' => $stoklar));
                                    ?>
                                </tbody>
                            </table>

                        </div>

                        </div>

                        </div>
                        <?php $this->load->view("dialogs/urundialog"); ?>
                        </body>
                        </html>