<form id="stokdurumfiltrefrm">
<div id="stokdurumfiltre" style="padding-bottom:3px;">
    
    Tarihler : <input type="text" name="stokdurum_baslangic_tarihi" id="stokdurum_baslangic_tarihi" class="input-medium" > &nbsp; - &nbsp;
               <input type="text" name="stokdurum_bitis_tarihi" id="stokdurum_bitis_tarihi" class="input-medium"> 
     &nbsp;
   Şehir : <select id="stokencoksatilan_sehir" name="stokencoksatilan_sehir" class="input-medium">
       <option value="">-- Seçiniz --</option>
          
       <?php echo $iller;?>
            </select>
   &nbsp;
   <input type="button" onclick="listeyenile();"  class="btn btn-info" value="Filtrele">

       
</div>
<div id="sonucsayisi" style="text-align: right;font-size: 12px;">
    <select id="limit_res" name="limit_res" class="input-small" onclick="listeyenile();">
        <option value="">Hepsi</option>
        <option value="10">10</option>
        <option value="50">50</option> 
        <option value="100">100</option> 
        <option value="1000">1000</option> 
    </select>
    Sonuç Göster
</div>
</form>
<div id="stokdurumu">
        
<table class="table table-bordered">
      <tr class="success">
          <td colspan="3" style="text-align:center;"><b>Stok Durum Raporu</b></td>   
    </tr>
    <tr class="info">
        <td>Stok Adı</td>
        <td>Stok Durumu</td>
        <td>Toplam Satış</td>
    </tr>
    <tbody id="tumhareketraporlari">
<?php 
foreach($toplamhareketler as $hareketrapor){
$this->load->view("stok_hareketleri/temelhareketlistpart",array('hareketrapor'=>$hareketrapor)); 
}
?>    
    </tbody>
</table>   
</div>