<!DOCTYPE HTML>

<html >
  <head>
    <meta charset="utf-8">
    <title><?php echo $this->lang->line("baslik_cikis");?></title>
<?php $this->load->view("standart");?>
<meta http-equiv="refresh" content="5;URL=<?php echo site_url();?>">
<script class="jsbin" src="<?php echo base_url().'js/tab.js';?>"></script>
<script class="jsbin" src="<?php echo base_url().'js/bootstrap-buttons.js';?>"></script>
<link href="<?php echo base_url().'css/bootstrap-responsive.css';?>" rel="stylesheet">
<link href="<?php echo base_url().'css/genel.css';?>" rel="stylesheet">

  </head>

  <body>
<?php $this->load->view("menusecimi");?>
<div class="container-fluid">
	<div class="row-fluid">
		<br><br>
	
			<div class="alert alert-info" style="width:82%;text-align:center;margin-left:9%;"><font size="3"><?php echo $this->lang->line("uyari_cikisyaptiniz");?></font><br><font size="2" face="arial"><?php echo $this->lang->line("uyari_anasayfayayonlendiriliyorsunuz")?></font>

			</div>
</div>

</div>

</body>
</html>