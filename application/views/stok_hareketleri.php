<!DOCTYPE HTML>

<html >
    <head>
        <meta charset="utf-8">
    <title>Stok Hareketleri</title>
    <?php $this->load->view("standart"); ?>
    <script type="text/javascript" src="<?php echo base_url() . 'js/stokhareketleri.js'; ?>"></script>	
    <script language="javascript">
        var site = "<?php echo site_url(); ?>";

        $(document).ready(function () {



            toplamfiyathesapla();
            $("#stok_event_product_count").keypress(function () {
                toplamfiyathesapla();
            });

            $("#stok_event_product_id,#stok_event_type").click(function () {
                toplamfiyathesapla();
            });

        });












    </script>
    <style>
        body {
            padding-top:50px;	
            width:80%;
            padding-left:10%;
        }
        .eklebtn{
            padding-bottom:15px;
        }
    </style>
</head>

<body>
    <?php //$this->load->view("menusecimi");?>
<div class="container-fluid">
    <div style="text-align:center;">
        <h4><u>Stok Hareketleri Yönetimi</u></h4>
    </div>
    <div class="row-fluid" style="">
        <div id="baslik">
            <div class="eklebtn">

                <a class="btn btn-warning btn-large" href="javascript:void(0)" onclick="stokhareketiekleduzenle('Ekle')"> Stok Hareketi Ekle  </a>
                &nbsp;<a href="<?php echo site_url(); ?>" class="btn btn-success btn-large">Ana Panele Dön</a>&nbsp;&nbsp;
            </div>
        </div>	
        <div id="stokhareketlerilistem">

            <table class="table table-bordered">
                <tr class="info">
                    <td>Stok Hareketi ID</td>
                    <td>Stok Hareket Tarihi</td>
                    <td>Stok Hareketi Tipi</td>
                    <td>Müşteri</td>
                    <td>İşlem Yapılan Ürün ve Miktar</td>
                    <td>Hareket Ücreti</td>		
                </tr>
                <tbody id="stokhareketlerilistesi">
                    <?php
                    $this->load->view("stok_hareketleri/stokhareketlist", array('stokhareketleri' => $stokhareketleri));
                    ?>
                </tbody>
            </table>

        </div>

    </div>

</div>
<?php $this->load->view("dialogs/stokhareketidialog"); ?>
</body>
</html>