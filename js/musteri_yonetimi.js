
function musteriekleduzenle(islem,musteri_id){
if(islem=="Ekle"){
	
$("#frmmusteri")[0].reset();
$("#musteri_adres").text("");	
}

$("#musteridiv").dialog({ 
modal: true,
buttons: [
    {
      text: islem,
      click: function() {
		  if(islem=="Ekle"){
			musteriekleprocess();
		  }else{
			 musteriduzenleprocess(2,musteri_id) ;
		  }
       // $( this ).dialog( "close" );
      }
    }
  ]});
//$(".ui-dialog-titlebar").hide();
$("#musteridiv" ).dialog( "option", "width", 500 );
$("#musteridiv" ).dialog( "option", "resizable", false );
$( "#musteridiv" ).dialog( "option", "closeOnEscape", true );
$( "#musteridiv" ).dialog( "option", "title", "Musteri "+islem );
$( "#musteridiv" ).dialog( "option", "closeText", "hide" );


}
function ilegoreilce(id,duzenlemesncid){
	
$.ajax({
		 url:site+'/stok/ilegoreilce/'+id,
		 type:"POST",
		
success:function(cikti){

		$("#"+duzenlemesncid).html(cikti);
	
	}
		
		 
		
 });	



 
}


function musteriekleprocess(){
$.ajax({
		 url:site+'/musteri/musteri_ekle/',
		 data:$("#frmmusteri").serialize(),
		 type:"POST",
		 success:function(cikti){
		 var snc=eval('('+cikti+')' );
		 
			
			if(snc["snc"]==true){
			 $("#frmmusteri")[0].reset();
			 listeyenile(2,snc["musteri_id"]);
			 $( "#musteridiv" ).dialog("close");
			}
			alert(snc["mesaj"]);
		 }
		
		 
		
 });		
	
	
}

function musteriduzenleprocess(asama,musteri_id){
if(asama==1){


$.ajax({
		 url:site+'/musteri/getmusteri/'+musteri_id,
		 type:"POST",
		 success:function(cikti){
			var snc=eval('('+cikti+')' );
		 
			
			if(snc["snc"]==true){
			 $("#musteri_adi").val(snc["musteri"]["musteri_name"]);
			 $("#musteri_tel").val(snc["musteri"]["musteri_tel"]);
			 $("#musteri_adres").text(snc["musteri"]["musteri_adres"]);
			 $("#musteri_ilce").val(snc["musteri"]["musteri_ilce"]);
			 $("#musteri_il").val(snc["musteri"]["musteri_il"]);
			 
			 
			 musteriekleduzenle("Düzenle",musteri_id);
			}else{
				alert(snc["mesaj"]);
			}
		 }
		
		 
		
 });

	
}else{
$.ajax({
		 url:site+'/musteri/musteri_duzenle/'+musteri_id,
		 data:$("#frmmusteri").serialize(),
		 type:"POST",
		 success:function(cikti){
		 var snc=eval('('+cikti+')' );
			
			if(snc["snc"]==true){
			 $("#frmmusteri")[0].reset();
			 listeyenile(1,musteri_id);
			 $( "#musteridiv" ).dialog("close");
			}
			alert(snc["mesaj"]);
		 }
	
 });	
} 
	
	
}

function musteri_sil(musteri_id){
$.ajax({
		 url:site+'/musteri/musteri_sil/'+musteri_id,
		 type:"POST",
		 success:function(cikti){
		 var snc=eval('('+cikti+')' );
		 
			
			if(snc["snc"]==true){
			 $("#mus_"+musteri_id).remove();
			
			}
			
			alert(snc["mesaj"]);
		 }
		
		 
		
 });		
	
}

function listeyenile(islem,musteri_id){
$.ajax({
		 url:site+'/musteri/getmusterilist/'+musteri_id,
                 data:$("#filtrefrm").serialize(),
		 type:"POST",
		 success:function(cikti){
                     
		 if(islem==2){
			$("#musterilistesi").append(cikti); 
		 }else if(islem==3){
                     $("#musterilistesi").html(cikti); 
                     
                 }else{
			 
			 $("#mus_"+musteri_id).replaceWith(cikti);
		 }
		 
		 
		 }
	
 });	
	
}


function musteriminiozet(musteri_id){
musteriozetgetir(musteri_id);
$("#musteriozet").dialog({ 
modal: true,
buttons: [
    {
      text: "Kapat",
      click: function() {
		
                     $( this ).dialog( "close" );
      }
    }
  ]});
//$(".ui-dialog-titlebar").hide();
$("#musteriozet" ).dialog( "option", "width", 800 );
$("#musteriozet" ).dialog( "option", "resizable", false );
$( "#musteriozet" ).dialog( "option", "closeOnEscape", true );
$( "#musteriozet" ).dialog( "option", "title", "Musteri Hesap Özeti" );
$( "#musteriozet" ).dialog( "option", "closeText", "hide" );


}

function musteriozetgetir(musteri_id){
$.ajax({
		 url:site+'/musteri/getmusteriozet/'+musteri_id,
		 type:"POST",
		 success:function(cikti){
             	 $("#musteriozet").html(cikti)
		 }
	
 });	
	
}

