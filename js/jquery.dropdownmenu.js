/**
 * jQuery Dropdown Menu Plugin
 *
 * Ramazan Terzi <rt@rterzi.com>
 * http://www.rterzi.com
 *
 */
(function( $ ) {
    
    $.fn.dropdown_menu = function(o){
        
        var settings = $.extend({
            'auto_hide'      : false,
            'save_state'     : true,
            'sub_menu_class' : 'sub',
            'ico_down_class' : 'ico_down',
            'ico_left_class' : 'ico_left'
        }, o);
                
        $(this).each(function(){
            
            var menu_ul = $(this);
            var sub_menus = menu_ul.find('.' + settings.sub_menu_class);
            
            sub_menus.hide();
            
            sub_menus.each(function(){
                
                var that = $(this);
                var menu_id = menu_ul.attr('id');
                
                $(this).parent().find('a:first').addClass('ico_left').click(function(){
                    
                    var siblings = $(this).parent().parent().find('li');
                    
                    if(settings.auto_hide) {
                        sub_menus.slideUp();
                        menu_ul.find('.' + settings.ico_down_class)
						       .removeClass(settings.ico_down_class)
							   .addClass(settings.ico_left_class);
                    }
                    
                    if(settings.save_state) {
                        
                        if (!that.is(':visible')) {
                            $.cookie('menu_order_div', menu_id, {path: '/'});
                            $.cookie('menu_order', siblings.index($(this).parent()), {path: '/'});
                        } else {
                            $.cookie('menu_order_div', '', {path: '/'});
                            $.cookie('menu_order', '', {path: '/'});
                        }
                        
                    }
                    
                    if (that.is(':visible')) {
                        that.slideUp();
                        $(this).removeClass(settings.ico_down_class)
						       .addClass(settings.ico_left_class);
                    } else {
                        that.slideDown();
                        $(this).removeClass(settings.ico_left_class)
						       .addClass(settings.ico_down_class);
                    }
                    return false;
                });
            });
            
        });
        
        if(settings.save_state && $.cookie('menu_order') && $.cookie('menu_order_div')) {
            var li_items = $('#' + $.cookie('menu_order_div')).find('li');
            var active_li = li_items[$.cookie('menu_order')];
            $(active_li).find('.' + settings.sub_menu_class)
                        .show()
                        .parent()
                        .find('a:first')
                        .removeClass(settings.ico_left_class)
                        .addClass(settings.ico_down_class);
        }
        
        
    }
    
})( jQuery );

// left menu
$(document).ready(function() {

    $('.left_menu').dropdown_menu();
    $('.left_most_wanted').dropdown_menu({auto_hide:true, save_state:false});

});