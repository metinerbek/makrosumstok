

$(function() {

  $('.btnNext').on('click', function() {        
    if(isLastTab()) 
      alert('submitting the form...');
    else 
      nextTab();  
  });
  
  $('a[data-toggle="tab"]').on('shown', function (e) {
    isLastTab();
  });

});

function nextTab() {
  var e = $('#tab li.active').next().find('a[data-toggle="tab"]');  
  if(e.length > 0) e.click();  
  isLastTab();
}

function isLastTab() {
  var e = $('#tab li:last').hasClass('active'); 
  if( e ) $('.btnNext').text('submit'); 
  else $('.btnNext').text('next step'); 
  return e;
}